﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EpubConverter
{
    public class Converter
    {
        static List<string> HtmlFolderNameList = new List<string> { "\\OPS", "\\OEBPS" };
        static List<string> HtmlFileExtensionList = new List<string> { ".htm", ".html", ".xhtml"};

        public static void Convert(string epubPath, string? newEpubFilename)
        {
            var newEpubPath = epubPath.Replace(".epub", "_MODIFIED.epub");
            if (newEpubFilename != null)
            {
                var indexOfSlash = epubPath.LastIndexOf('\\');
                newEpubPath = epubPath.Substring(0, indexOfSlash + 1) + newEpubFilename;
            }

            //Extract the files and create the directory
            ExtractEpub(epubPath);

            //Grab a list of the valid files in the .epub that contain readable text
            //NOTE: This uses the same logic as in ExtractEpub() to get the directory path here
            var directoryPath = epubPath.Remove(epubPath.IndexOf(GetExtension(epubPath)));
            var fileList = GetValidFiles(directoryPath);
            if (fileList.Count == 0)
            {
                throw new Exception("Uh oh, I don't see any files I can modify");
            }

            foreach (var file in fileList)
            {
                ConvertFile(file);
                if (File.Exists(file.Replace(GetExtension(file), "_old" + GetExtension(file))))
                {
                    File.Delete(file.Replace(GetExtension(file), "_old" + GetExtension(file)));
                }
            }

            var newZipPath = newEpubPath.Replace(".epub", ".zip");

            if (File.Exists(newZipPath))
            {
                File.Delete(newZipPath);
            }
            ZipFile.CreateFromDirectory(directoryPath, newZipPath);
            if(File.Exists(newEpubPath))
            {
                File.Delete(newEpubPath);
            }
            File.Move(newZipPath, newEpubPath);

            try
            {
                //Delete the working zip file and directory
                if (File.Exists(directoryPath + ".zip"))
                {
                    File.Delete(directoryPath + ".zip");
                }
                Directory.Delete(directoryPath, true);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private static void ExtractEpub(string path)
        {
            var fileName = path;

            //Get the file name and extension of the file
            var endOfPathIndex = path.Length - 1;
            if (fileName.Contains('/'))
            {
                var indexOfSlash = path.LastIndexOf('/');
                fileName = path.Substring(indexOfSlash + 1, endOfPathIndex - indexOfSlash);
            }
            if (fileName.Contains('\\'))
            {
                var indexOfSlash = path.LastIndexOf('\\');
                fileName = path.Substring(indexOfSlash + 1, endOfPathIndex - indexOfSlash);
            }

            var extension = GetExtension(path);
            if (extension != ".epub")
            {
                throw new Exception("That's not an .epub file. How did this happen? They're supposed to only be able to select .epub files");
            }

            //Create the new path for the .zip file, as well as the path for the extracted directory
            var zipPath = path.Replace(extension, ".zip");
            var directoryPath = path.Remove(path.IndexOf(extension));

            //Create the zip file if it doesn't already exist, and create the new folder if it doesn't already exist. 
            try
            {
                if (!File.Exists(zipPath))
                {
                    File.Copy(path, zipPath);
                }
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
            }
            catch (IOException copyError)
            {
                Console.WriteLine(copyError.Message);
            }

            //If the folder is not empty, extract to the folder, and then delete the .zip file
            try
            {
                var directoryFileCount = Directory.GetDirectories(directoryPath).Length + Directory.GetFiles(directoryPath).Length;
                if (directoryFileCount == 0)
                {
                    ZipFile.ExtractToDirectory(zipPath, directoryPath);
                    File.Delete(zipPath);
                }
            }
            catch (IOException unzipError)
            {
                MessageBox.Show(unzipError.Message);
            }
            catch (UnauthorizedAccessException authError)
            {
                MessageBox.Show(authError.Message);
            }
        }

        private static void ConvertFile(string filePath)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            try
            {
                xmlDoc.Load(filePath);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw e;
            }

            var pList = xmlDoc.GetElementsByTagName("p");
            foreach(XmlNode p in pList)
            {
                var inner = ConvertLine(p.InnerXml);
                p.InnerXml = inner;
            }
            xmlDoc.Save(filePath);
        }
        private static string ConvertLine(string line)
        {
            if(line == "")
            {
                return line;
            }

            var workingLine = line;

            var words = workingLine.Split(' ').ToList();
            var convertedLine = "";
            var tagWord = "";
            foreach(var word in words)
            {
                //TODO: Split the words again if they have a - in them (Example: empty-handed)
                if (!string.IsNullOrEmpty(tagWord))
                {
                    tagWord = tagWord + " " + word;
                    //If the tag ends, and another one does not begin
                    if (word.Contains('>') && word.LastIndexOf('>') > word.LastIndexOf('<'))
                    {
                        var newTagWord = ConvertWord(tagWord);
                        convertedLine = convertedLine + " " + newTagWord;
                        tagWord = "";
                    }
                    continue;
                }
                if ((word.Contains('<') && !word.Contains('>')) || (word.Contains('<') && word.Contains('>') && word.LastIndexOf('<') > word.LastIndexOf('>')))
                {
                    tagWord = word;
                    continue;
                }
                var newWord = ConvertWord(word);
                convertedLine = convertedLine + " " + newWord;
            }

            return convertedLine.Trim();
        }

        private static string ConvertWord(string word, int preTagLen = 0, int totalLen = 0)
        {
            short count = 0;
            var workingWord = word;
            var preTagString = "";

            //If the totalLen has not been set, then we are at the start of the word.
            var tagCount = 0;
            if (totalLen == 0)
            {
                foreach (var character in workingWord)
                {
                    if (character == '<')
                    {
                        tagCount++;
                    }
                }
                for (int i = 0; i < tagCount; i++)
                {
                    var startInd = workingWord.IndexOf('<');
                    var endInd = workingWord.IndexOf('>');
                    workingWord = workingWord.Remove(startInd, endInd - startInd + 1);
                }
                totalLen = workingWord.Length;
                workingWord = word;
            }

            int target = (totalLen / 2) + (totalLen % 2);

            if (totalLen == 3)
                target = 1;

            if (preTagLen > target)
            {
                return word;
            }

            //If the word already has a tag in it, we gotta do more work
            if (word.Contains('<'))
            {
                //If there is any text before the tags, pull that out and add the bold tags based on the total length, and any previous preTag length.
                //Also, remove the opening tags from the word, adding them to the preTagString
                if (workingWord.IndexOf('<') != 0)
                {
                    preTagString = workingWord.Substring(0, workingWord.IndexOf('<'));
                    preTagString = ConvertWord(preTagString, preTagLen, totalLen);
                    preTagLen += workingWord.IndexOf('<');
                    workingWord = workingWord.Remove(0, workingWord.IndexOf('<'));
                }

                while (workingWord.IndexOf('<') == 0)
                {
                    preTagString = preTagString + workingWord.Substring(0, workingWord.IndexOf('>') + 1);
                    workingWord = workingWord.Remove(0, workingWord.IndexOf('>') + 1);
                }
                if (!string.IsNullOrEmpty(workingWord))
                {
                    workingWord = ConvertWord(workingWord, preTagLen, totalLen);
                }
                return preTagString + workingWord;
            }

            //Iterate through the actual word, ending the bold section halfway through
            //If there is any text before tags in this word, add its length to this check

            //If the word is one character, or this is part of the first half of a word, return the full tag
            if (workingWord.Length + preTagLen <= target)
            {
                return "<b>" + workingWord + "</b>";
            }

            var newWord = "<b>";
            foreach (var letter in workingWord)
            {
                if(count == target - preTagLen)
                {
                    newWord = newWord + "</b>";
                }
                newWord = newWord + letter;
                count++;
            }

            return preTagString + newWord;
        }

        //Helper Functions
        private static List<string> GetValidFiles(string directoryPath)
        {
            //Check for the proper folder
            var htmlFileDirectory = directoryPath;
            foreach (var possibleFolder in HtmlFolderNameList)
            {
                if (Directory.Exists(directoryPath + possibleFolder))
                {
                    htmlFileDirectory = htmlFileDirectory + possibleFolder;
                    break;
                }
            }

            var directoryFiles = Directory.GetFiles(htmlFileDirectory);
            var validFiles = new List<string>();

            foreach (var file in directoryFiles)
            {
                var extension = GetExtension(file);
                if (HtmlFileExtensionList.Contains(extension) && !file.Contains("_old"))
                {
                    validFiles.Add(file);
                }
            }

            return validFiles;
        }

        private static string GetExtension(string path)
        {
            return path.Substring(path.LastIndexOf("."), path.Length - path.LastIndexOf("."));
        }
    }
}
