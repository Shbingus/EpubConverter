using System.Security;

namespace EpubConverter
{
    public partial class EpubConverterForm : Form
    {
        public EpubConverterForm()
        {
            InitializeComponent();
        }
        //private Button openFileButton;
        //private OpenFileDialog openFileDialog1;
        //private TextBox textBox1;

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void openFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Epub Files (*.epub)|*.epub";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    SetText(openFileDialog1.FileName);
                    button2.Enabled = true;
                }
                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }


        private void SetText(string text)
        {
            textBox1.Text = text;

            var endOfPathIndex = text.Length - 1;
            var indexOfSlash = text.LastIndexOf('\\');
            textBox2.Text = text.Substring(indexOfSlash + 1, endOfPathIndex - indexOfSlash).Replace(".epub", "_MODIFIED.epub");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!(textBox2.Text.Substring(textBox2.Text.Length - 5) == ".epub"))
            {
                MessageBox.Show("The Output file needs to be in the .epub format.");
            }
            else
            {
                Converter.Convert(openFileDialog1.FileName, textBox2.Text);
            }
        }
    }
}